import java.io.*;
import agenda.ControlePessoa;
import agenda.Pessoa;
import java.util.Scanner;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;
    
    

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    ControleTelefone umControleTelefone = new ControleTelefone();
    Pessoa umaPessoa = new Pessoa();
    
    Scanner input = new Scanner(System.in);
    
    //variaveis de controle do menu
    int menuPrincipal, menuSecundario;

    //interagindo com usuario
    do {
        System.out.println("Escolha uma opção:");
        System.out.println("1 - Adicionar");
        System.out.println("2 - Remover");
        System.out.println("3 - Pesquisar");
        System.out.println("4 - Sair");
        
        menuPrincipal = input.hashCode();
        
        switch (menuPrincipal){
            
            case 1:
                do{
                    System.out.println("Escolha uma opção a ser Adicionada:");
                    System.out.println("1 - Nome");
                    System.out.println("2 - Telefone");
                    System.out.println("3 - Voltar");
                    
                    menuSecundario = input.hashCode();
                    
                    switch (menuSecundario){
                        case 1:
                            System.out.println("Digite o nome:");
                            entradaTeclado = leitorEntrada.readLine();
                            String umNome = entradaTeclado;
                            umaPessoa.setNome(umNome);
                            break;
                        case 2:
                            System.out.println("Digite o telefone:");
                            entradaTeclado = leitorEntrada.readLine();
                            String umTelefone = entradaTeclado;
                            umaPessoa.setTelefone(umTelefone);
                            break;
                        default:
                            System.out.println("Digite qualquer tecla para voltar ao menu iniciar");
          
                    }
                 }while(1 == menuSecundario || 2 == menuSecundario);
                 break;
            case 2:
                 do{
                    System.out.println("Escolha uma opção a ser Removido:");
                    System.out.println("1 - Nome");
                    System.out.println("2 - Telefone");
                    System.out.println("3 - Voltar");
                    
                    menuSecundario = input.hashCode();
                    
                    switch (menuSecundario){
                        case 1:
                           System.out.println("Digite o nome a ser removido:");
 	                   entradaTeclado = leitorEntrada.readLine();
                           String nomeRemover = entradaTeclado; 	
                           umControle.remover(umControle.pesquisar(nomeRemover));
                            break;
                        case 2:
                           System.out.println("Digite o telefone a ser removido:");
 	                   entradaTeclado = leitorEntrada.readLine();
                           String telefoneRemover = entradaTeclado; 	
                           umControle.remover(umControle.pesquisar(telefoneRemover));
                           break;
                        default:
                            System.out.println("Digite qualquer tecla para voltar ao menu iniciar");
          
                    }
                 }while( 1 == menuSecundario || 2 == menuSecundario );
                 break;
            case 3:
                do{
                    System.out.println("Escolha uma opção a ser Pesquisada:");
                    System.out.println("1 - Nome");
                    System.out.println("2 - Telefone");
                    System.out.println("3 - Voltar");
                    
                    menuSecundario = input.hashCode();
                    
                    switch (menuSecundario){
                        case 1:
                            System.out.println("Digite o nome para pesquisar:");
                            entradaTeclado = leitorEntrada.readLine();
                            String nomePesquisa = entradaTeclado;
                            umaPessoa = umControle.pesquisar(nomePesquisa);
                            System.out.println("Encontrada, nome: " +umaPessoa.getNome(nomePesquisa));
                            break;
                        case 2:
                            System.out.println("Digite o nome para pesquisar:");
                            entradaTeclado = leitorEntrada.readLine();
                            String telefonePesquisa = entradaTeclado;
                            Pessoa umTelefone = umControle.pesquisar(telefonePesquisa);
                            if(umTelefone == null){
                               System.out.println("Nada foi encontrado!");
                            }else
                               System.out.println("Encontrado, telefone: " +umTelefone.getNome(telefonePesquisa));
                             break;
                        default:
                            System.out.println("Digite qualquer tecla para voltar ao menu iniciar");
          
                    }
                 }while(1 == menuSecundario || 2 == menuSecundario);
                 break;
                    }
                
                
        }
  
    //adicionando uma pessoa na lista de pessoas do sistema
    String mensagem = umControle.adicionar(umaPessoa);

    //adicionando um telefone na lista
    String mensagemPhone = umControle.adicionar(umTelefone);

    //adicionando um Idade na lista
    String mensagemIdade = umControle.adicionar(umaIdade);

    //adicionando um email na lista
    String mensagemEmail = umControle.adicionar(umEmail);
    

    //conferindo saida
    System.out.println("========================================");
    System.out.println("Nome: " +mensagem);
    System.out.println("Telefone: " +mensagemPhone);
    System.out.println("Idade: " +mensagemIdade);
    System.out.println("Telefone: " +mensagemEmail);

  
  }

}

