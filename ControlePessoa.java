/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package agenda;

/**
 *
 * @author Iolane Caroline
 */
import java.util.ArrayList;

public class ControlePessoa {

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// metodos
   
    public String adicionar(Pessoa umaPessoa) {
        String mensagem = "Pessoa adicionada com Sucesso!";
	listaPessoas.add(umaPessoa);
	return mensagem;
    }

    public void remover(Pessoa umPessoa) {
	String mensagem = "Pessoa removida";
        listaPessoas.remove(umPessoa);
	return mensagem;
    }
    
    public Pessoa pesquisar(String nome) {
        for (Pessoa pessoa: listaPessoas) {
            if (pessoa.getNome().equalsIgnoreCase(nome)) 
	       return pessoa;
        }
        return null;
    }
    
    public String adicionarTelefone(Pessoa umTelefone) {
      String mensagem = "Telefone adicionado com sucesso!";
      listaPessoas.add(umTelefone);
      return mensagem;
   }
	
   public void removerTelefone(Pessoa umTelefone) {
      listaPessoas.remove(umTelefone);
   }

   public Pessoa pesquisarTelefone(String telefone) {
      for (Pessoa telefone : listaPessoas) {
         if (pessoa.getTelefone())
            return pessoa;
      }
      return null;
   }


}

